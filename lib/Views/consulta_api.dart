import 'package:api_mysql_app/Models/get_all_data.dart';
import 'package:api_mysql_app/Views/detail_page.dart';
import 'package:flutter/material.dart';

class ConsultaApi extends StatefulWidget {
  ConsultaApi({Key key}) : super(key: key);

  @override
  _ConsultaApiState createState() => _ConsultaApiState();
}

class _ConsultaApiState extends State<ConsultaApi> {
  
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getClientes(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        List _snap = snapshot.data;

        if(snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if(snapshot.hasError) {
          return Center(
            child: Container(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Error recopilando la data",
                        style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold)
                      ),
                    ],
                  ),
                  RaisedButton(
                    onPressed: (){
                      FutureBuilder(
                        future: getAllData(),
                      );
                    },
                    textColor: Colors.white,
                    padding: const EdgeInsets.all(0.0),
                    child: Container(
                      color: Colors.teal,
                      padding: const EdgeInsets.all(10.0),
                      child: const Text(
                        'RETRY', 
                        style: TextStyle(
                          fontSize: 20
                        )
                      ),
                    ),
                  ),
                ],
              )
            ),
          );
        }
        return ListView.builder(
          padding: const EdgeInsets.all(16.0),
          itemCount: _snap.length,
          itemBuilder: (context,index) {
            // Colocar una linea debajo del dato
            if (index.isOdd) return Divider();

            // Extraer solo 10 registro por vex
            final i = index ~/ 2; /*3**/
            if (i >= _snap.length) {
              _snap.take(10); /*4*/
            }

            // Mostrar los datos en pantalla
            // return ListTile(
            //   title: Text("${_snap[index]['nombre']}"),
            //   subtitle: Text("Cedula: ${_snap[index]['cedula']}"),
            //   trailing: Icon(
            //     Icons.favorite_border,
            //     color: Colors.red,
            //   ),
            //   onTap: () {
            //     setState( () {

            //     });
            //   },
            // );
            return ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                  snapshot.data[index].imagen,
                ),
              ),
              title: Text(snapshot.data[index].nombre),
              subtitle: Text(snapshot.data[index].correo),
              onTap: () {
                // Navigator.push(context,
                //   new MaterialPageRoute(builder: (context) => DetailPage(snapshot.data[index]))
                // );
                Navigator.push(context,
                  new MaterialPageRoute(builder: (context) => DetailPage())
                );
              }
            );
          }
        );
      }
    );
  }
}