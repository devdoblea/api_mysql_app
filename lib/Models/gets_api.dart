import 'dart:async';
import 'package:http/http.dart' as http;

const baseUrl = "http://devdoblea.000webhostapp.com/api";

class ApiGetList {
  static Future getUsers() {
    var url = baseUrl + "/index";
    return http.get(url);
  }
}

class ApiGetSingle {
  static Future getUsers() {
    var url = baseUrl + "/fetch_single";
    return http.get(url);
  }
}