// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'clientes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Clientes _$ClientesFromJson(Map<String, dynamic> json) {
  return Clientes(
    json['idper'] as String,
    json['nombre'] as String,
    json['cedula'] as String,
    json['imagen'] as String,
    json['sexo'] as String,
    json['direccion'] as String,
    json['tlfnocel'] as String,
    json['tlfnocas'] as String,
    json['correo'] as String,
    json['cargoId'] as String,
    json['statusper'] as String,
    json['fechain'] as String,
    json['operador'] as String,
  );
}

Map<String, dynamic> _$ClientesToJson(Clientes instance) => <String, dynamic>{
      'idper': instance.idper,
      'nombre': instance.nombre,
      'cedula': instance.cedula,
      'imagen': instance.imagen,
      'sexo': instance.sexo,
      'direccion': instance.direccion,
      'tlfnocel': instance.tlfnocel,
      'tlfnocas': instance.tlfnocas,
      'correo': instance.correo,
      'cargoId': instance.cargoId,
      'statusper': instance.statusper,
      'fechain': instance.fechain,
      'operador': instance.operador,
    };
