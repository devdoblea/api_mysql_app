import "package:http/http.dart" as http;
import 'dart:convert';
import 'package:api_mysql_app/Models/clientes.dart';

getAllData() async {
  final String theUrl = "http://devdoblea.000webhostapp.com/api/index";
  var res = await http.get(Uri.encodeFull(theUrl));
  
  var responseBody = json.decode(res.body);

  print(responseBody);

  return responseBody;
}

Future<List<Clientes>> getClientes() async {

  final String theUrl = "http://devdoblea.000webhostapp.com/api/index";

  var response = await http.get(Uri.encodeFull(theUrl));
  
  if (response.statusCode == 200) {

    var clientesMap = jsonDecode(response.body);

    //var clientes = Clientes.fromJson(clientesMap);
    // inicializo como una lista el valor de User para poder mostrarlo
    List<Clientes> clientes = [];

    //recorreo el array para llenar variables con los datos traidos
    for(var c in clientesMap) {

      Clientes cliente = Clientes(
        c["idper"], 
        c["nombre"], 
        c["cedula"], 
        c["imagen"],
        c["sexo"],
        c["direccion"], 
        c["tlfnocel"], 
        c["tlfnocas"], 
        c["correo"], 
        c['cargoId'], 
        c["statusper"], 
        c["fechain"], 
        c["operador"]
      );

      clientes.add(cliente);
    }

      print("Response Body: "+response.body);

      print(clientes.length); // Imprimo por consola el valor length de users

      return clientes;

  } else {

    print("Request failed with status: ${response.statusCode}.");

  }

  print("Request failed with status: ${response.statusCode}.");
  return null;

}