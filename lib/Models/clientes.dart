import 'package:json_annotation/json_annotation.dart';

/// This allows the "Cliente" class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'clientes.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()

class Clientes {

  Clientes(
    this.idper,
    this.nombre,
    this.cedula,
    this.imagen,
    this.sexo,
    this.direccion,
    this.tlfnocel,
    this.tlfnocas,
    this.correo,
    this.cargoId,
    this.statusper,
    this.fechain,
    this.operador,
  );

  String idper;
	String nombre;
	String cedula;
  String imagen;
	String sexo;
	String direccion;
	String tlfnocel;
	String tlfnocas;
	String correo;
	String cargoId;
	String statusper;
	String fechain;
	String operador;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated "_$ClienteFromJson()" constructor.
  /// The constructor is named after the source class, in this case, Cliente.
  factory Clientes.fromJson(Map<String, dynamic> json) => _$ClientesFromJson(json);

  /// 'toJson' is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method '_$UserToJson'.
  Map<String, dynamic> toJson() => _$ClientesToJson(this);
}