class Cliente {
  int idper;
	String nombre;
	String cedula;
  String imagen;
	String sexo;
	String direccion;
	String tlfnocel;
	String tlfnocas;
	String correo;
	int cargoId;
	String statusper;
	DateTime fechain;
	String operador;

  // Cliente(int id, String nombre, String cedula, String sexo, String direccion, String tlfnocel, String tlfnocas, String correo, int cargoId, String statusper, DateTime fechain, String operador) {
  //   this.id;
  //   this.nombre;
  //   this.cedula;
  //   this.sexo;
  //   this.direccion;
  //   this.tlfnocel;
  //   this.tlfnocas;
  //   this.correo;
  //   this.cargoId;
  //   this.statusper;
  //   this.fechain;
  //   this.operador;
  // }

  Cliente(
    this.idper,
    this.nombre,
    this.cedula,
    this.imagen,
    this.sexo,
    this.direccion,
    this.tlfnocel,
    this.tlfnocas,
    this.correo,
    this.cargoId,
    this.statusper,
    this.fechain,
    this.operador
  );

  Cliente.fromJson(Map json)
      : idper     = json['idper'],
        nombre    = json['nombre'],
        cedula    = json['cedula'],
        imagen    = json['imagen'],
        sexo      = json['sexo'],
        direccion = json['direccion'],
        tlfnocel  = json['tlfnocel'],
        tlfnocas  = json['tlfnocas'],
        correo    = json['correo'],
        cargoId   = json['cargoId'],
        statusper = json['statusper'],
        fechain   = json['fechain'],
        operador  = json['operador'];

  Map<String, dynamic> toJson() {
    return {'idper': idper, 'nombre': nombre, 'cedula': cedula, 'imagen': imagen, 'sexo': sexo, 'direccion': direccion, 'tlfnocel': tlfnocel, 'tlfnocas': tlfnocas, 'correo': correo, 'cargoId': cargoId, 'statusper': statusper, 'fechain': fechain, 'operador': operador};
  }
}